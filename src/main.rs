use image::io::Reader as ImageReader;
use image::GenericImageView;
use std::io::prelude::*;
use std::net::TcpStream;
use std::time::Instant;
use tracing::Level;
use tracing_subscriber;

fn main() -> std::io::Result<()> {
    tracing_subscriber::fmt()
        // filter spans/events with level TRACE or higher.
        .with_max_level(Level::TRACE)
        .init();
    tracing::info!("smap");

    let img = ImageReader::open("wahla.png")?.decode().unwrap();

    let img_width = img.width();
    tracing::info!("width: {img_width}");

    let img_height = img.height();
    tracing::info!("height: {img_height}");

    let mut pn_pic = String::new();

    for y in 0..img_height {
        for x in 0..img_width {
            let imgpx = img.get_pixel(x, y);

            let r = imgpx.0[0];
            let g = imgpx.0[1];
            let b = imgpx.0[2];

            let pnpx = format!("PX {x} {y} {:x?}{:x?}{:x?}\n", r, g, b);
            pn_pic.push_str(pnpx.as_str());
        }
    }

    //let cmd = "SIZE\n";
    //let cmd = "PX 23 42 00ff00\nPX 15 45 00ff00\n";
    let buffer = pn_pic.as_bytes();
    let start = Instant::now();
    let mut stream = TcpStream::connect("127.0.0.1:1337")?;
    stream.write(buffer)?;
    let duration = start.elapsed().as_millis();
    let bsize = buffer.len();
    tracing::info!("send {bsize} bytes in {duration} ms",);

    Ok(())
}
